var React = require('react');
var AdDetails = require('./AdDetails');
import axios from 'axios';
import { ORDER_COLUMNS} from '../constants/apiurls';
import {Link} from 'react-router-dom';

class Discover extends React.Component {
	constructor(props) {
    super(props);

    this.state = {
      url: '/AdDetails/',
      advPosts: [],
      userData: {}
    };
    //this.fetchAds = this.fetchAds.bind(this);
   }

    fetchAds(){
    	if(this.props.location && this.props.location.state && this.props.location.state.vData && this.props.location.state.vData !=''){
    		console.log(this.props.location.state.vData);
    		var vData = JSON.parse(this.props.location.state.vData);

    		this.props.location.state.vData = '';

    		axios.post(ORDER_COLUMNS.getAdsByVidId,{
		      	"videoId" : vData.vdId,
				"startTime" : vData.sTime
		      })
		        .then(res => {
		          console.log("response:"+res.data);
		          const advPosts = res.data;
		          this.setState({ advPosts });
		        });

    	}else{

    		 axios.get(ORDER_COLUMNS.getAds)
		      .then(res => {
				  const advPosts = res.data;
		          this.setState({ advPosts });
		      });

    	}
    }

    addToWishlist(e , id){
    	console.log(id);
    	const userData = JSON.parse(localStorage.getItem('userData'));
    	axios.post(ORDER_COLUMNS.addToWishlist,{
		      	"username" : userData.username,
				"adId" : id
		      })
		        .then(res => {
		          console.log("response:"+res.data);
		          //const advPosts = res.data;
		          this.fetchUserInfo();
		          this.fetchAds();
		        });
    	
    	e.preventDefault();
    }
    removeFromWishlist(e , id){
    	console.log(id);
    	const userData = JSON.parse(localStorage.getItem('userData'));
    	axios.post(ORDER_COLUMNS.removeFromWishlist,{
		      	"username" : userData.username,
				"adId" : id
		      })
		        .then(res => {
		          console.log("response:"+res.data);
		          //const advPosts = res.data;
		          this.fetchUserInfo();
		          this.fetchAds();
		        });
    	
    	e.preventDefault();
    }
    fetchUserInfo(){
    	axios.get(ORDER_COLUMNS.getUserByUname + '/tushargarg')
		      .then(res => {
				  const userData = res.data;
				  console.log(userData);
				  localStorage.setItem('userData', JSON.stringify(userData));
		          this.setState({ userData });
		      });
    }
    componentDidMount() {
    	
		this.fetchUserInfo();
    	this.fetchAds();
    }


   render() {
      return (
		<div>
			{this.state.advPosts.map(post => 
				<Link to={this.state.url+post._id} key={post._id}>
					<div>
						<div className='image-container'>
							<img src={post.thumbnail}/>
						</div>
						<div className="profile-header">
							<div className="profile-pic-container"><img className='profile-pic-icon' src="../images/profile-icon.png" /></div>
							<div className="profile-description">
								<div className="name-container">{post.title}</div>
								<div className="desc-container">{post.description}</div>
							</div>
							{(this.state.userData.wishlistAds.indexOf(post._id) != -1) ? (								
								<img className="add-to-wishlist-icon" src="../images/remove-wishlist-icon.png" onClick={(e) => this.removeFromWishlist(e, post._id)}/>
							):(
							<img className="add-to-wishlist-icon" src="../images/wishlist-icon.png" onClick={(e) => this.addToWishlist(e, post._id)}/>
								
							)}
						
							
						</div>
					</div>
				</Link>
			)}
		</div>
      );
   }
}

module.exports = Discover;