var React = require('react');
import axios from 'axios';
import { ORDER_COLUMNS, CATEGORIES_TimeStamp, CATEGORIES_ARR} from '../constants/apiurls'

class Categories extends React.Component {

	constructor(props) {
    super(props);

    this.state = {
      posts: CATEGORIES_ARR.categories,
      cat: "Bollywood",
      subCat: ""
    };
  }

  componentDidMount() {
    // axios.get(ORDER_COLUMNS.getCategories)
    //   .then(res => {

    //     const posts = res.data;
    //     this.setState({ posts });
    //   });

    var labels = CATEGORIES_TimeStamp.labels;
    var categories = CATEGORIES_ARR.categories;
    var s= localStorage.sTime;
    var e = localStorage.eTime
    var cats = [];
    var posts = {};
    for(var i=0; i<labels.length; i++){
      for(var j=0; j<Object.keys(labels[i].locations).length; j++){
        var start = labels[i].locations[j][0];
        var end = labels[i].locations[j][1];
        if(start != -1 || end != -1){
          if(start <=s && end>=s){ // right overlap
            cats.push(labels[i].description);
            break;
          }
          else if(start <=e && end<=e){ //right overlap
            cats.push(labels[i].description);
            break;
          }
          else if(start>=s && end <=e){ //totaloverlap
            cats.push(labels[i].description);
            break
          }
        }
        
      }
    }

    console.log(cats);

    for (var key in categories) {
      if (categories.hasOwnProperty(key)) {
        for(var i=0; i<categories[key].length; i++){
          if(cats.indexOf(categories[key][i]) != -1){
            if(posts[key] == undefined)
              posts[key] = [];
            posts[key].push(categories[key][i]);
          }
        }
      }
    }

    this.setState({posts});
    var cat = Object.keys(posts)[0];
    this.setState({ cat });
    console.log(posts);

  }

  setActiveCat(cat) {
    this.setState({ cat });
  }

  setActiveSubCat(subCat) {
    this.setState({ subCat });
  }

  proceedToDetails(){
    localStorage['cat'] = this.state.cat;
    localStorage['subCat'] = this.state.subCat;
    //console.log(localStorage);
    window.location.href = "/AdDetails";  
  }

   render() {
      return (
    		<div className='categories-container'>
            <div className="categories">
              <h4>Please select category: </h4>
          		{Object.entries(this.state.posts).map(([key, post]) =>
                <div className={ this.state.cat == key ? 'active': 'categories-wrapper' } key={key}>
                  <div className="cat-container" >
            				<div onClick={this.setActiveCat.bind(this, key)}>{key}</div>
            			</div>
                </div>
              )}
              <div className="cleardiv"></div>
            </div>
            <div className="sub-categories">
              <h4>Please select sub-category: </h4>
              {this.state.posts[this.state.cat].map(catg =>
                <div className={ this.state.subCat == catg ? 'active': 'categories-wrapper' } key={catg}>
                  <div className="sub-cat-container" onClick={this.setActiveSubCat.bind(this, catg)}>{catg}</div>
                </div>
              )}
              <div className="cleardiv"></div>
            </div>
            <div className="align-center">
              <button onClick="" className="white-btn">Back</button>
              <button onClick={this.proceedToDetails.bind(this)} className="blue-btn">Next</button>
            </div>
    		</div>
      );
   }
}

module.exports = Categories;