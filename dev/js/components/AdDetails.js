var React = require('react');
import axios from 'axios';
import { ORDER_COLUMNS} from '../constants/apiurls';
import {Link} from 'react-router-dom';

class AdDetails extends React.Component {
	constructor(props) {
    super(props);
    this.state = {
        'adDetails' : {
          'pName' : '',
          'for' : '',
          'pDesc' : '',
          'url' : '',
          'pPrice' : ''
        },
        'price' : 0
     }
   }

   handleChange(propName , e){
    const adDetails = this.state.adDetails;
    adDetails[propName] = e.target.value;
    this.setState({ adDetails });
   }

   proceedToPay(){
    localStorage['pName'] = this.state.adDetails.pName;
    localStorage['for'] = this.state.adDetails.for;
    localStorage['pDesc'] = this.state.adDetails.pDesc;
    localStorage['url'] = this.state.adDetails.url;
    localStorage['pPrice'] = this.state.adDetails.pPrice;
    window.location.href = "/CheckoutPreview";  
   }

   componentDidMount() {
      this.setState({price:parseInt(localStorage.adDuration)*50});
    }

   render() {
      return (
    		<div className="product-details-container">
          <h3>Product Details</h3>
          <div className="details-container">
            <div className="left-float">
              <div>
                <label>Product Name</label><br/>
                <input type="text" className="txt-fld" onChange={this.handleChange.bind(this,'pName')} placeholder="Enter your Product Name"/>
              </div>

              <div>
                <label>For</label><br/>
                <input type="radio" name="productFor" onChange={this.handleChange.bind(this,'for')} value="Female" /><span>Female</span>
                <input type="radio" name="productFor" onChange={this.handleChange.bind(this,'for')} value="Male" /><span>Male</span>
                <input type="radio" name="productFor" onChange={this.handleChange.bind(this,'for')} value="Unisex" /><span>Unisex</span>
              </div>

              <div>
                <label>Product Description</label><br/>
                <textarea onChange={this.handleChange.bind(this,'pDesc')}  placeholder="Enter Product Description here"/>
              </div>

              <div>
                <label>URL</label><br/>
                <input type="text" onChange={this.handleChange.bind(this,'url')} className="txt-fld" placeholder="Enter your URL here"/>
              </div>
            </div>
            <div className="left-float">
              <div>
                <label>Product Price</label><br/>
                <input type="text" className="txt-fld" onChange={this.handleChange.bind(this,'pPrice')} placeholder="Enter your Product Price"/>
              </div>
              
              <div>
                <label>Select Thumbnail</label><br/>
                <input type="file" name="pic" accept="image/*"/>
              </div>
            </div>
            <div className="cleardiv"></div>
            <div className="align-right amt-container">
              <span>Amount Payable:</span><span> &#8377; {this.state.price}</span> 
            </div>
            <div className="align-right">
              <button onClick="" className="white-btn">Back</button>
              <button onClick={this.proceedToPay.bind(this)} className="blue-btn">Submit and Pay Ad Fee</button>
            </div>
          </div>
    		</div>
      );
   }
}

module.exports = AdDetails;