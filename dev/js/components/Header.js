import ReactDOM from "react-dom";
var React = require('react');

class Header extends React.Component {
   render() {
      return (
         <div className='header'>
           <span className="menu-container"><img className='logo-icon' src="../images/assets/svgs/logo.svg" /> AdGence</span>
           <span className="search-container"><img className='search-icon' src="../images/assets/svgs/search-icon.svg" /></span>
         </div>
      );
   }
}

module.exports = Header;