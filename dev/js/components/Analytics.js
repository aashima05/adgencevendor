var React = require('react');
var LineChart = require('zingchart-react').line;
const Highcharts = require('highcharts');
const mapData = require('./us-all-maps').US_ALL;
const _spline = require('./spline-data').SPLINE_DATA;
const ReactHighcharts = require('react-highcharts'); // Expects that Highcharts was loaded in the code.
var ReactHighmaps = require('react-highcharts/ReactHighmaps.src');

const _data = [{ "value": 438, "code": "nj" }, { "value": 387.35, "code": "ri" }, { "value": 312.68, "code": "ma" }, { "value": 271.4, "code": "ct" }, { "value": 209.23, "code": "md" }, { "value": 195.18, "code": "ny" }, { "value": 154.87, "code": "de" }, { "value": 114.43, "code": "fl" }, { "value": 107.05, "code": "oh" }, { "value": 105.8, "code": "pa" }, { "value": 86.27, "code": "il" }, { "value": 83.85, "code": "ca" }, { "value": 72.83, "code": "hi" }, { "value": 69.03, "code": "va" }, { "value": 67.55, "code": "mi" }, { "value": 65.46, "code": "in" }, { "value": 63.8, "code": "nc" }, { "value": 54.59, "code": "ga" }, { "value": 53.29, "code": "tn" }, { "value": 53.2, "code": "nh" }, { "value": 51.45, "code": "sc" }, { "value": 39.61, "code": "la" }, { "value": 39.28, "code": "ky" }, { "value": 38.13, "code": "wi" }, { "value": 34.2, "code": "wa" }, { "value": 33.84, "code": "al" }, { "value": 31.36, "code": "mo" }, { "value": 30.75, "code": "tx" }, { "value": 29, "code": "wv" }, { "value": 25.41, "code": "vt" }, { "value": 23.86, "code": "mn" }, { "value": 23.42, "code": "ms" }, { "value": 20.22, "code": "ia" }, { "value": 19.82, "code": "ar" }, { "value": 19.4, "code": "ok" }, { "value": 17.43, "code": "az" }, { "value": 16.01, "code": "co" }, { "value": 15.95, "code": "me" }, { "value": 13.76, "code": "or" }, { "value": 12.69, "code": "ks" }, { "value": 10.5, "code": "ut" }, { "value": 8.6, "code": "ne" }, { "value": 7.03, "code": "nv" }, { "value": 6.04, "code": "id" }, { "value": 5.79, "code": "nm" }, { "value": 3.84, "code": "sd" }, { "value": 3.59, "code": "nd" }, { "value": 2.39, "code": "mt" }, { "value": 1.96, "code": "wy" }, { "value": 0.42, "code": "ak" }];
const _data_ = [{ "value": 1, code: 'CO', name: 'COLORADO' }]
const config = {
	chart: {
		borderWidth: 1
	},

	title: {
		text: ''
	},

	legend: {
		enabled: false,
		layout: 'horizontal',
		borderWidth: 0,
		backgroundColor: '#cccccc',
		floating: false,
		verticalAlign: 'top',
		y: 25
	},

	mapNavigation: {
		enabled: true
	},

	colorAxis: {
		min: 1,
		type: 'logarithmic',
		minColor: '#3892ff',
		maxColor: '#3892ff',
		stops: [
			[1, '#3892ff']
		]
	},

	series: [{
		animation: {
			duration: 1000
		},
		data: _data_,
		mapData: mapData,
		joinBy: ['postal-code', 'code'],
		dataLabels: {
			enabled: true,
			color: '#FFFFFF',
			format: '{point.code}'
		},
		name: 'Population density',
		tooltip: {
			pointFormat: '{point.name}'
		}
	}]
};

const _lineConfig = {
	rangeSelector: {
		selected: 1
	},

	 xAxis: {
	    categories: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']
	},

	title: {
		text: ''
	},

	series: [{
		name: 'Accessories',
		data: _spline,
		type: 'spline'
	},{
		name: 'Electronics',
		data: [['Monday',30],['Tuesday',44],['Wednesday',25],['Thursday',25],['Friday',41],['Saturday',26],['Sunday',103]],
		type: 'spline'
	},{
		name: 'Apparels',
		data: [['Monday',60],['Tuesday',44],['Wednesday',21],['Thursday',55],['Friday',48],['Saturday',54],['Sunday',135]],
		type: 'spline'
	}
	]
};

const myLineValues = [
	{ text: "First Series", values: [0, 1, 2, 2, 4, 6, 7] },
	{ text: "Second Series", values: [18, 12, 7, 14, 1, 19, 4] },
	{ text: "Third Series", values: [0, 1, 12, 12, 4, 6, 17] },
	{ text: "Fourth Series", values: [18, 22, 17, 4, 1, 9, 4] },
	{ text: "Fifth Series", values: [4, 2, 7, 3, 23, 7, 2] },
	{ text: "Sixth Series", values: [10, 6, 8, 2, 6, 3, 9] }
];

class Analytics extends React.Component {
	componentDidMount() {
		_data.map(e => {
			e.code = e.code.toUpperCase();
		});
		config.data = _data;
	}
	render() {
		return (
			<div className="ad-graphs">
				<div className="master-plot card drop-shadow minimal">
					<div className="title-text">Category approach statistics</div>
					<ReactHighcharts config={_lineConfig} />
				</div>
				<div className="quick-glance card">
					<div className="card drop-shadow minimal aligned-center">
						<div className="title-text">
							User who wishlisted your Ads
						</div>
						<div className="ad-count">
							4000
						</div>
					</div>
					<div className="card drop-shadow minimal aligned-center">
						<div className="title-text">
							User who reached to your site
						</div>
						<div className="ad-count">
							2300
						</div>
					</div>
					<div className="card drop-shadow minimal aligned-center">
						<div className="title-text">
							User who reached to your site
						</div>
						<div className="ad-count">
							23
						</div>
					</div>
				</div>
				
				<div className="overall-graph card drop-shadow minimal">
					<ReactHighmaps config={config} />
				</div>
			</div>
		);
	}
}

module.exports = Analytics;