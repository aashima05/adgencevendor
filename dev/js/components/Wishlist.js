var React = require('react');
import axios from 'axios';
import { ORDER_COLUMNS} from '../constants/apiurls';

class Wishlist extends React.Component {
	constructor(props) {
    super(props);

    this.state = {
      advPosts: [
      	{	
      		'_id': 1,
      		'url': 'https://static.pexels.com/photos/248797/pexels-photo-248797.jpeg',
      		'companyName': 'Abcdefgh i',
      		'productName': 'Testing 1',
      		'desc': 'something . something'
      	},
      	{
      		'_id': 2,
      		'url': 'http://www.patagonia.com/dis/dw/image/v2/ABBM_PRD/on/demandware.static/-/Sites-patagonia-master/default/dw2997321b/images/hi-res/27805_COI_OPEN.jpg?sw=750&sh=750&sm=fit&sfrm=png',
      		'companyName': 'xyz',
      		'productName': 'mnop',
      		'desc': 'Testing testing testing 4'
      	}
      ]
    };
   }
   fetchWishlistAds(){
    const userData = JSON.parse(localStorage.getItem('userData'));
      axios.post(ORDER_COLUMNS.fetchWishlistAds,{
            "wishlistAds" : userData.wishlistAds,
          })
            .then(res => {
              console.log("response:"+res.data);
              const advPosts = res.data;
              this.setState({ advPosts });
          });
            }
    componentDidMount() {
      
    //this.fetchUserInfo();
    
      this.fetchWishlistAds();
    }
   render() {
      return (
		<div>
			{this.state.advPosts.map(post => 
				<div key={post._id}>
					<div className='image-container'>
						<img src={post.thumbnail}/>
					</div>
					<div className="profile-header">
						<div className="profile-pic-container"><img className='profile-pic-icon' src="../images/profile-icon.png" /></div>
						<div className="profile-description">
							<div className="name-container">{post.productName}/{post.companyName}</div>
							<div className="desc-container">{post.desc}</div>
						</div>
						<img className="add-to-wishlist-icon" src="../images/remove-wishlist-icon.png" />
					</div>
				</div>
			)}
		</div>
      );
   }
}

module.exports = Wishlist;