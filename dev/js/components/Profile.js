var React = require('react');

class Profile extends React.Component {
   render() {
      return (
		<div>
			<div className="profile-header">
				<div className="profile-pic-container"><img className='profile-pic-icon' src="../images/profile-icon.png" /></div>
				<div className="profile-description">
					<div className="name-container">Fist LastName</div>
					<div className="desc-container">some blah blah description</div>
				</div>
				<button className="white-btn edit-profile-btn">Edit profile</button>
			</div>
			<div className="user-options">
				<div>
					<img src="../images/history-icon.png"/>
					<span>My History</span>
				</div>
				<div>
					<img src="../images/updates-icon.png"/>
					<span>Updates</span>
				</div>
				<div>
					<img src="../images/help-supprt-icon.png"/>
					<span>Help & Support</span>
				</div>
				<div>
					<img src="../images/invite-icon.png"/>
					<span>Invite</span>
				</div>
				<div>
					<img src="../images/setting-icon.png"/>
					<span>Settings</span>
				</div>
			</div>
			<div className="signout-container">
				<button className="blue-btn">Sign Out</button>
			</div>
		</div>
      );
   }
}

module.exports = Profile;