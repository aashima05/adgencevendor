var React = require('react');
import {MICROSEC_CONVERTER} from '../constants/apiurls'

class HomePage extends React.Component {

	constructor(props) {
	    super(props);
	    
	    this.state = {
	      adDuration: '5',
	      sTime: 0
	    };
	}

	handleInputChange(e){
		this.setState({ sTime: e.target.value });
	}

	handleRadioChange(e){
		this.setState({ adDuration: e.target.value });
	}

	proceedToCategories(){
		localStorage['sTime'] = parseInt(this.state.sTime)*MICROSEC_CONVERTER.micro_factor;
		localStorage['eTime'] = (parseInt(this.state.sTime)+parseInt(this.state.adDuration))*MICROSEC_CONVERTER.micro_factor;
		localStorage['adDuration'] = this.state.adDuration;
		//console.log(localStorage);
		window.location.href = "/Categories";
	}

	myFunction(e){
		console.log(this);
		var vid = document.getElementById('player1');
		document.getElementById('start-time').value = Math.floor(vid.currentTime);
		this.setState({ sTime: Math.floor(vid.currentTime) });
	}

   render() {
      return (
		<div className="home-page">
			<div className="video-container">
				<video id="player1" controls onPause={this.myFunction.bind(this)} onPlay={this.myFunction.bind(this)} onSeeking={this.myFunction.bind(this)}>
				  <source src="https://storage.googleapis.com/adgencevideos/vidID0.mp4" type="video/mp4"/>
				  Your browser does not support the video tag.
				</video>
			</div>
			<div>
				<p>Choose your Ad duration</p>
				<div>
					<input type="radio" name="duration" checked={this.state.adDuration === '5'} onChange={this.handleRadioChange.bind(this)} value="5" id="5sec" /><label htmlFor="5sec">5 Seconds</label>
					<input type="radio" name="duration" checked={this.state.adDuration === '8'} onChange={this.handleRadioChange.bind(this)} value="8" id="8sec"/><label htmlFor="8sec">8 Seconds</label>
					<input type="radio" name="duration" checked={this.state.adDuration === '12'} onChange={this.handleRadioChange.bind(this)} value="12" id="12sec"/><label htmlFor="12sec">12 Seconds</label>
					<input type="radio" name="duration" checked={this.state.adDuration === '15'} onChange={this.handleRadioChange.bind(this)} value="15" id="15sec"/><label htmlFor="15sec">15 Seconds</label>
				</div>
				<p>Enter Start Time(in seconds)</p>
				<div>
					<input type="number" min="0" onChange={this.handleInputChange.bind(this)} id="start-time"/>
				</div>
				<div className="align-center">
					<button className="blue-btn" onClick={this.proceedToCategories.bind(this)}>Next</button>
				</div>
			</div>
		</div>
      );
   }
}

module.exports = HomePage;