import React, { Component } from 'react'
import {MICROSEC_CONVERTER} from '../constants/apiurls'

class CheckoutPreview extends React.Component {

    constructor(props){
      super(props);
      this.state = {
        'sTime' : parseInt(localStorage.sTime)/MICROSEC_CONVERTER.micro_factor,
        'adDuration' : localStorage.adDuration,
        'cat' : localStorage.cat,
        'subCat' : localStorage.subCat,
        'pName' : localStorage.pName,
        'for' : localStorage.for,
        'pDesc' : localStorage.pDesc,
        'url' : localStorage.url,
        'pPrice' : localStorage.pPrice
      }
    }
 
      render(){ 
        return(
          <div className="product-details-container">
          <h3>Review</h3>
          <div className="review-container">
            <div>
              <label>Product Name: </label>{this.state.pName}
            </div>

            <div>
              <label>For : </label>{this.state.for}
            </div>

            <div>
              <label>Product Description : </label>{this.state.pDesc}
            </div>

            <div>
              <label>URL : </label>{this.state.url}
            </div>
            <div>
              <label>Product Price : </label>{this.state.pPrice}
            </div>
            <div className="align-right amt-container">
              <span>Amount Payable:</span><span> &#8377; 253</span> 
            </div>
            <div className="align-right">
              <button onClick="" className="white-btn">Back</button>
              <button onClick="" className="blue-btn">Pay</button>
            </div>
          </div>
        </div>
        )
    }
}

module.exports = CheckoutPreview;