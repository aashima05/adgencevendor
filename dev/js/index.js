import 'babel-polyfill';
import React from 'react';
import ReactDOM from "react-dom";
import { BrowserRouter, Route, browserHistory, NavLink, Link } from 'react-router-dom'

var HomePage = require('./components/HomePage');
var CheckoutPreview = require('./components/CheckoutPreview');
var Categories = require('./components/Categories');
var Header = require('./components/Header');
var Footer = require('./components/Footer');
var AdDetails = require('./components/AdDetails');
var Profile = require('./components/Profile');
var Analytics = require('./components/Analytics');
var zingChart = require('zingchart');

const Highcharts = require('highcharts'); // Expects that Highcharts was loaded in the code.

class App extends React.Component {
	render() {
		return (
			<BrowserRouter history={browserHistory}>
				<div>
					<Header />
					<div className="custom-wrapper">
						<div className="ad-main-container">
							<div className="ad-Nav">
								<NavLink className="ad-nav-link drop-shadow" activeClassName="activated" disabled={true} to={`/Profile`}>
									<span>Profile</span>
								</NavLink >
								<NavLink className="ad-nav-link drop-shadow" activeClassName="activated" disabled={true} to={`/History`}>History</NavLink>
								<NavLink className="ad-nav-link drop-shadow" activeClassName="activated" to={`/Analytics`}>Analytics</NavLink>
								<NavLink className="ad-nav-link drop-shadow" activeClassName="activated" disabled={true} to={`/Support`}>Support</NavLink>
								<NavLink className="ad-nav-link drop-shadow" activeClassName="activated" disabled={true} to={`/Settings`}>Settings</NavLink>
							</div>
							<div className="ad-content-container">
								<Route exact path='/' component={HomePage}></Route>
								<Route path='/CheckoutPreview' component={CheckoutPreview}></Route>
								<Route path='/Categories' component={Categories}></Route>
								<Route path='/AdDetails' component={AdDetails}></Route>
								<Route path='/Profile' component={Profile}></Route>
								<Route path='/Analytics' component={Analytics}></Route>
							</div>
						</div>

					</div>
				</div>
			</BrowserRouter>
		);
	}
}


ReactDOM.render(<App />, document.getElementById('root')
);
